#include "commons.h"
#include "calculator.h"

int main(int argc, char**argv) {
  Fl::scheme("gtk+");
  Calculator* cal = new Calculator(0,0,400,600, "Calculator");
  cal->show(argc, argv);
  return Fl::run();
}
