# FLCAL

flcal is a calculator written in C++ using the [FLTK](https://www.fltk.org/) library  
for the graphical user interface

## How to use it

To build flcal, you will need `make` to be installed on your system  
The compiler that is used is `gcc` so adjust the `Makefile` as needed
