#pragma once

#include "commons.h"

class MyButton : public Fl_Button {
  public:
    MyButton(int x, int y, int w, int h, const char* label);
};

class Calculator : public Fl_Window {
  Fl_Group *buttons;
  Fl_Input *result;
  MyButton *b0, *b1, *b2, *b3, *b4, *b5, *b6, *b7, *b8, *b9,
           *clear, *dot, *plus, *minus, *multiply, *divide, 
           *power, *equals;

  public:
    Calculator(int x, int y, int w, int h, const char* label);
    ~Calculator();
};
