#include "operators.h"

void calculate(string& res, string lab, Fl_Input* result) {
  if (op == '+') {
    leftval = to_string(stod(leftval) + stod(rightval));
  } else if (op == '-') {
    leftval = to_string(stod(leftval) - stod(rightval));
  } else if (op == '*') {
    leftval = to_string(stod(leftval) * stod(rightval));
  } else if (op == '/') {
    leftval = to_string(stod(leftval) / stod(rightval));
  } else if (op == '^') {
    leftval = to_string(pow(stod(leftval), stod(rightval)));
  }

  // Remove the unesecarry zeros
  size_t last = leftval.length();
  bool at_least_one;
  while (leftval[last - 1] == '0') {
    --last;
    at_least_one = true;
  }

  // If there was at least one unesecarry zero
  if (at_least_one) {
    // Check if everything was a zero
    // and then remove the dot
    if (leftval[last - 1] == '.')
      --last;

    string new_last;
    for (size_t i = 0; i < last; i++) {
      new_last += leftval[i];
    } leftval = new_last;
  }

  res = leftval + lab[0];
  result->value(res.c_str());
  dot_used = false;
  rightval = "";
}

void op_cb(Fl_Widget* w, void* e) {
  Fl_Input* result = (Fl_Input*)e;
  string res = result->value();
  string lab = w->label();

  // Find why the fuck I used the second statment
  if (dot_used && res[res.length() - 1] == '.') {
    fl_alert("The dot is pressed, expecting number");
    return;
  }

  if (op != '\0') {
    if (rightval.length() == 0) {
      fl_alert("Operator in use, expecting number");
      return;
    }
    calculate(res, lab, result);
    op = lab[0];
  } else {
    op = lab[0];
    res = res + lab[0];
    result->value(res.c_str());
    dot_used = false;
  }
}

void equals_cb(Fl_Widget* w, void* e) {
  Fl_Input* result = (Fl_Input*)e;
  string res = result->value();
  string lab = w->label();

  if (op != '\0' && rightval.length() == 0) {
    fl_alert("Operator in use, expecting number");
    return;
  }

  if (rightval.length() == 0) {
    fl_alert("There is not an expression to be calculated");
    return;
  }

  if (dot_used && res[res.length() - 1] == '.') {
    fl_alert("The dot is pressed, expecting number");
    return;
  }

  calculate(res, "", result);
  op = '\0';
}
