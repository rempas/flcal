#include "commons.h"
#include "calculator.h"
#include "callbacks.h"
#include "operators.h"

MyButton::MyButton(int x, int y, int w, int h, const char* label) : Fl_Button(x,y,w,h,label) {
  this->box(FL_UP_BOX);
  this->labelfont(FL_ITALIC);
  this->labelsize(27);
  this->labeltype(FL_EMBOSSED_LABEL);
}

Calculator::Calculator(int x, int y, int w, int h, const char* label) : Fl_Window(x,y,w,h,label) {
  result = new Fl_Input(2, 2, 396, 60, "");

  b0 = new MyButton(2,510,90,90,"0");
  b1 = new MyButton(100,510,90,90,"1");
  b2 = new MyButton(198,510,90,90,"2");
  b3 = new MyButton(296,510,90,90,"3");

  b4 = new MyButton(2,400,90,90,"4");
  b5 = new MyButton(100,400,90,90,"5");
  b6 = new MyButton(198,400,90,90,"6");
  b7 = new MyButton(296,400,90,90,"7");

  b8 = new MyButton(2,290,90,90,"8");
  b9 = new MyButton(100,290,90,90,"9");
  dot = new MyButton(198,290,90,90,".");
  clear = new MyButton(296,290,90,90,"C");

  plus = new MyButton(2,180,90,90,"+");
  minus = new MyButton(100,180,90,90,"-");
  multiply = new MyButton(198,180,90,90,"*");
  divide = new MyButton(296,180,90,90,"/");

  power = new MyButton(2,70,90,90,"^");
  equals = new MyButton(100,70,90,90,"=");

  b0->callback(zero_cb, result);
  b0->shortcut('0');

  b1->callback(get_num_cb, result);
  b1->shortcut('1');

  b2->callback(get_num_cb, result);
  b2->shortcut('2');

  b3->callback(get_num_cb, result);
  b3->shortcut('3');

  b4->callback(get_num_cb, result);
  b4->shortcut('4');

  b5->callback(get_num_cb, result);
  b5->shortcut('5');

  b6->callback(get_num_cb, result);
  b6->shortcut('6');

  b7->callback(get_num_cb, result);
  b7->shortcut('7');

  b8->callback(get_num_cb, result);
  b8->shortcut('8');

  b9->callback(get_num_cb, result);
  b9->shortcut('9');

  dot->callback(dot_cb, result);
  dot->shortcut('.');

  clear->callback(clear_cb, result);
  clear->shortcut(FL_BackSpace);
  plus->callback(op_cb, result);
  plus->shortcut('+');
  minus->callback(op_cb, result);
  minus->shortcut('-');
  multiply->callback(op_cb, result);
  multiply->shortcut('*');
  divide->callback(op_cb, result);
  divide->shortcut('/');
  power->callback(op_cb, result);
  power->shortcut('^');
  equals->callback(equals_cb, result);
  equals->shortcut('=');

  this->resizable(this);
  this->size_range(400, 350, 500, 700);
  this->end();
}

Calculator:: ~Calculator() {}
