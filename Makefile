all: obj/flcal.o obj/calculator.o obj/callbacks.o obj/operators.o
	c++ obj/*.o -I/usr/include /usr/lib/libfltk.so.1.3 -o flcal

obj/flcal.o: flcal.cpp
	c++ flcal.cpp -o obj/flcal.o -c -Os

obj/calculator.o: calculator.cpp
	c++ calculator.cpp -o obj/calculator.o -c -Os

obj/callbacks.o: callbacks.cpp
	c++ callbacks.cpp -o obj/callbacks.o -c -Os

obj/operators.o: operators.cpp
	c++ operators.cpp -o obj/operators.o -c -Os

clean: $(TARGET) $(OBJS)
	rm -f obj/*.o flcal 2> /dev/null
