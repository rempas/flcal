#include "commons.h"
#include "callbacks.h"

string leftval;
string rightval;
char op = '\0';
bool dot_used;

void zero_cb(Fl_Widget* w, void* e) {
  Fl_Input* result = (Fl_Input*)e;
  if (op == '\0')
    if (leftval.length() == 0) {
      fl_alert("The first number can't be '0'");
      return;
    } else
      leftval += '0';
  else
    if (rightval.length() == 0) {
      fl_alert("The first number can't be '0'");
      return;
    } else
      rightval += '0';

  result->value(string(result->value() + string("0")).c_str());
}

void get_num_cb(Fl_Widget* w, void* e) {
  Fl_Input* result = (Fl_Input*)e;
  // Hold the string literal to
  // be able to safely compare it
  string lab = w->label();

  if (op == '\0')
    leftval += lab;
  else
    rightval += lab;

  // For some reason we can't combine them in the same line
  string res = result->value();
  res = res + lab;
  result->value(res.c_str());
}

void dot_cb(Fl_Widget* w, void* e) {
  if (dot_used) {
    fl_alert("The dot has already been used");
    return;
  }

  if (op == '\0')
    if (leftval.length() == 0) {
      fl_alert("Expecting number before dot");
      return;
    } else
      leftval += '.';
  else
    if (rightval.length() == 0) {
      fl_alert("Expecting number before dot");
      return;
    } else
      rightval += '.';

  // Check if after using equals (=)
  // the final number has a dot in it
  // The rightval must also not exist
  if (rightval.length() == 0) {
    for (size_t i = 0; i < leftval.length(); i++) {
      if (leftval[i] == '.') {
        fl_alert("The dot has already been used");
        return;
      }
    }
  }

  dot_used = true;

  Fl_Input* result = (Fl_Input*)e;
  string res = result->value();
  res = res + '.';
  result->value(res.c_str());
}

void clear_cb(Fl_Widget* w, void* e) {
  leftval = "";
  rightval = "";
  dot_used = false;
  op = '\0';
  ((Fl_Input*)e)->value("");
}
