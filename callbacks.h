#pragma once

#include "commons.h"

void zero_cb(Fl_Widget* w, void* e);
void get_num_cb(Fl_Widget* w, void* e);
void dot_cb(Fl_Widget* w, void* e);
void clear_cb(Fl_Widget* w, void* e);
void plus_cb(Fl_Widget* w, void* e);
void minus_cb(Fl_Widget* w, void* e);
void multiply_cb(Fl_Widget* w, void* e);
void divide_cb(Fl_Widget* w, void* e);
void power_cb(Fl_Widget* w, void* e);
