#pragma once

#include "commons.h"

extern bool dot_used;
extern char op;
extern string leftval, rightval;

void op_cb(Fl_Widget* w, void* e);
void equals_cb(Fl_Widget* w, void* e);
